﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Menu : MonoBehaviour {

    public UnityEngine.UI.Toggle toggle;

    public GameObject howToWindow;
    public GameObject creditsWindow;

    void Start()
    {
        ChangePitchSetting();
        howToWindow.SetActive(true);
        creditsWindow.SetActive(false);
    }

    public void ChangePitchSetting()
    {
        int value = (toggle.isOn) ? 1 : 0;
        PlayerPrefs.SetInt("pitch", value);
    }

    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }


    public void ToggleCredits()
    {
        creditsWindow.SetActive(!creditsWindow.activeSelf);
        howToWindow.SetActive(!howToWindow.activeSelf);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

}
