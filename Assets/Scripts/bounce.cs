﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bounce : MonoBehaviour {
    private float y = 0, fo, time=0;
    
	// Use this for initialization
	void Start () {
        fo = transform.localPosition.y;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        time += Time.deltaTime;
        y = fo + (Mathf.Sin(time)/10f);
        transform.localPosition = new Vector3(transform.localPosition.x, y, transform.localPosition.z);
	}
}
