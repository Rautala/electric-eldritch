﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectWaver : MonoBehaviour {
    private Mesh _mesh;
    public Vector3[] origVerts, currentVerts;

    float time = 0f;

	// Use this for initialization
	void Start () {
        _mesh = GetComponent<MeshFilter>().mesh;
        origVerts = _mesh.vertices;
        currentVerts = origVerts;
        time = Random.Range(0.5f, 2.5f);
	}
	
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;
        WaveAffect();
	}

    float a = 0;
    private void WaveAffect()
    {
        for(int i=0;i<origVerts.Length;i++)
        {
            a = (Mathf.Sin(time) * (Mathf.Pow(origVerts[i].y,2)))/800f;

            currentVerts[i] = new Vector3(origVerts[i].x - a, origVerts[i].y, origVerts[i].z);
        }
        _mesh.vertices = currentVerts;
        _mesh.RecalculateBounds();
    }
}
