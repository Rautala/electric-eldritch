﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[System.Serializable]
public class SpectrumRangeClass
{
	public int minIndex;
	public int maxIndex;
	public float thresholdUpValue;
	public float thresholdDownValue;
}

public class SpectrumAnalyzer : MonoBehaviour
{
	private AudioSource source;
	
	public List<SpectrumRangeClass> spectrumRangeClassList = new List<SpectrumRangeClass> ();
	public List<float> maxValue = new List<float> ();

	[SerializeField]float[] spectrum;
	public int samples = 128;
	public float scaleMulti = 1f;

	public Vector3 scale = new Vector3 (1f, 1f, 1f);

	void Start () {
		spectrum = new float[samples];
		source = GetComponent<AudioSource> ();

        StartCoroutine(CalculateSpectrum());

    }

    List<int> overThresholdIndexList = new List<int>();
    private float[] freqData;
    private float fMax;
    private float fLow;
    private float fHigh;
    /*
    IEnumerator CalculateSpectrum()
    {
        fMax = AudioSettings.outputSampleRate / 2;

        while (true)
        {
            fLow = Mathf.Clamp(fLow, 20, fMax);
            fHigh = Mathf.Clamp(fHigh, fLow, fMax);

            source.GetSpectrumData(spectrum, 0, FFTWindow.BlackmanHarris);
            int n1 = Mathf.FloorToInt(fLow * samples / fMax);
            int n2 = Mathf.FloorToInt(fHigh * samples / fMax);

            float sum = 0f;

            for (int i = n1; i <= n2; i++)
            {
                sum += spectrum[i];
            }

            print("n1 " + n1 + "; n2 " + n2 + "; sum " + sum +  ";  freg " + (sum / (n2 - n1 + 1)));

            yield return null;
        }
    }
        
        function BandVol(fLow:float, fHigh:float): float {

             fLow = Mathf.Clamp(fLow, 20, fMax); // limit low...
             fHigh = Mathf.Clamp(fHigh, fLow, fMax); // and high frequencies
             // get spectrum
             audio.GetSpectrumData(freqData, 0, FFTWindow.BlackmanHarris); 
             var n1: int = Mathf.Floor(fLow* nSamples / fMax);
             var n2: int = Mathf.Floor(fHigh* nSamples / fMax);
             var sum: float = 0;
             // average the volumes of frequencies fLow to fHigh
             for (var i = n1; i<=n2; i++){
                 sum += freqData[i];
             }
             return sum / (n2 - n1 + 1);
         }

            */
    
    IEnumerator CalculateSpectrum () {
        float average = 0f;

        float pitch = 0f;
        float averagePitch = 0f;
        int averagePitchCount = 0;

        int value = 0;
        int max = 0;

        if (lr != null)
        {
            SetupLine();
        }

        while (true) {
            //source.GetSpectrumData (spectrum, 0, FFTWindow.BlackmanHarris);
            source.GetOutputData(spectrum, 0);

            for (int i = 0; i < spectrumRangeClassList.Count; i++) {
                SpectrumRangeClass range = spectrumRangeClassList [i];
                average = GetSoundRegionValue(range);
                overThresholdIndexList.Clear();
                for (int j = 0; j < spectrum.Length; j++)
                {
                    if (spectrum[j] > average)
                    {                        
                        overThresholdIndexList.Add(j);
                    }
                }
            }

            if (lr != null && wavePoints.Length > 0)
            {
                Wave();
            }

            if (overThresholdIndexList.Count > 0)
            {
                max = overThresholdIndexList.Max();
                value = overThresholdIndexList.BinarySearch(max);
            }

            if (MicControl.instance.loudness > 0.1f)
            {
                pitch += value;
            }
            else
            {
                pitch = 0;
            }

            averagePitchCount++;

            if (averagePitchCount == 20)
            {
                averagePitch = pitch / averagePitchCount;
                averagePitchCount = 0;
                pitch = 0f;

                print("Average pitch of last 10 frames " + averagePitch * (22050 / samples));
            }

            //print("max value index " + value);

            yield return new WaitForFixedUpdate();
        }
    }
    
    Vector3[] wavePoints;
    float step = 0.005f;
    public LineRenderer lr;

    private void SetupLine()
    {
        wavePoints = new Vector3[spectrumRangeClassList[0].maxIndex];
        for (int i = 0; i < spectrumRangeClassList[0].maxIndex; i++)
        {
            wavePoints[i] = new Vector3(i * step, 0f, 0f);
        }
        lr.numPositions = wavePoints.Length;
        lr.SetPositions(wavePoints);
    }

    private void Wave()
    {
        for (int i = 0; i < spectrumRangeClassList[0].maxIndex; i++)
        {
            wavePoints[i] = new Vector3((i * step), spectrum[i] * 50, 0f);
        }
        lr.SetPositions(wavePoints);
    }


    public void OnLevelStart () {
        StartCoroutine (CalculateSpectrum ());
		GetComponent<AudioSource> ().Play ();
	}
    
	float GetSoundRegionValue (SpectrumRangeClass range) {
		float value = 0;
		int min = (range.minIndex > 0) ? range.minIndex + 1 : 0;
		int max = (range.maxIndex > 0) ? range.maxIndex + 1 : 0;
		for (int i = min; i < max; i++) {
			value += spectrum [i];
		}
		return value;
	}

    int IndexAverage(List<int> indexList)
    {
        int allIndexValues = 0;
        foreach (int i in indexList)
        {
            allIndexValues += i;
        }

        return Mathf.CeilToInt(allIndexValues / indexList.Count);
    }


	float GetSoundRegionAverage (SpectrumRangeClass range) {
		float value = 0;

		int min = (range.minIndex > 0) ? range.minIndex + 1 : 0;
		int max = (range.maxIndex > 0) ? range.maxIndex + 1 : 0;

		for (int i = 0; i < max; i++) {
			value += spectrum [i];
		}
//		print (value / (max - min));
		return value / (max - min);
	}
}
