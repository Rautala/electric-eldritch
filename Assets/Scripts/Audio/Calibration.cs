﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class Calibration : MonoBehaviour {

    public float minPitchValue;
    public float maxPitchValue;

    public float minVolumeValue;
    public float maxVolumeValue;

    public Text minPitchValueText;
    public Text maxPitchValueText;

    public Text minVolumeValueText;
    public Text maxVolumeValueText;

    public Image calibrationTimer;

    public Button calibratePitchMinButton;    
    public Button calibratePitchMaxButton;

    public Button calibrateVolumeMinButton;
    public Button calibrateVolumeMaxButton;

    public void StartPitchCalibration(string valueName)
    {        
        if (valueName == "min" || valueName == "max")
        {
            StartCoroutine(CalibratePitch(valueName));
        }
        else
        {
            Debug.LogWarning("Wrong 'valueName' in " + this);
        }
    }
    

    IEnumerator CalibratePitch(string valueName)
    {
        print("start to calibrate pitch " + valueName);

        float timePassed = 0f;
        float extremeValue = 0f;
        float currentValue = 0f;
        float value = 0;

        int count = 0;

        MicControl.instance.StartMicrophone();

        calibratePitchMinButton.gameObject.SetActive(false);
        calibratePitchMaxButton.gameObject.SetActive(false);
        calibrateVolumeMinButton.gameObject.SetActive(false);        
        calibrateVolumeMaxButton.gameObject.SetActive(false);

        while (timePassed < 10f)
        {
            value = AudioAnalyzer.instance.AnalyzeSound();
            print("value " + value);
            if (value > 0)
            {
                currentValue += value;
                count++;
            }

            calibrationTimer.fillAmount = timePassed / 10f;
            timePassed += Time.deltaTime;
            yield return null;
        }

        extremeValue = currentValue / count;

        MicControl.instance.StopMicrophone();
        calibratePitchMinButton.gameObject.SetActive(true);
        calibratePitchMaxButton.gameObject.SetActive(true);
        calibrateVolumeMinButton.gameObject.SetActive(true);
        calibrateVolumeMaxButton.gameObject.SetActive(true);

        if (valueName == "min")
        {
            minPitchValue = extremeValue;
            minPitchValueText.text = minPitchValue.ToString("0000");
            print("new min value is " + minPitchValue);
            //AudioAnalyzer.instance.minFreq = extremeValue;
        }
        else if (valueName == "max")
        {
            maxPitchValue = extremeValue;
            maxPitchValueText.text = maxPitchValue.ToString("0000");
            print("new max value is " + maxPitchValue);
            //AudioAnalyzer.instance.maxFreq = extremeValue;
        }
        else
        {
            Debug.LogWarning("Wrong 'valueName' in " + this);
        }
    }


    public void StartVolumeCalibration(string valueName)
    {
        if (valueName == "min" || valueName == "max")
        {
            StartCoroutine(CalibrateVolume(valueName));
        }
        else
        {
            Debug.LogWarning("Wrong 'valueName' in " + this);
        }
    }

    IEnumerator CalibrateVolume(string valueName)
    {
        print("start to calibrate volume " + valueName);

        float timePassed = 0f;
        float extremeValue = 0f;
        float currentValue = 0f;
        float value = 0;

        int count = 0;

        MicControl.instance.StartMicrophone();

        calibratePitchMinButton.gameObject.SetActive(false);
        calibratePitchMaxButton.gameObject.SetActive(false);
        calibrateVolumeMinButton.gameObject.SetActive(false);
        calibrateVolumeMaxButton.gameObject.SetActive(false);

        while (timePassed < 3f)
        {
            value = MicControl.instance.loudness;
            print("value " + value + "; loudness " + MicControl.instance.loudness);

            if (MicControl.instance.loudness > 0.2f)
            currentValue += value;
            count++;

            calibrationTimer.fillAmount = timePassed / 3f;
            timePassed += Time.deltaTime;
            yield return null;
        }

        extremeValue = currentValue / count;

        MicControl.instance.StopMicrophone();
        calibratePitchMinButton.gameObject.SetActive(true);
        calibratePitchMaxButton.gameObject.SetActive(true);
        calibrateVolumeMinButton.gameObject.SetActive(true);
        calibrateVolumeMaxButton.gameObject.SetActive(true);

        if (valueName == "min")
        {
            minVolumeValue = extremeValue;
            minVolumeValueText.text = minVolumeValue.ToString("0000");
            print("new min value is " + minVolumeValue);
            //AudioAnalyzer.instance.minFreq = extremeValue;
        }
        else if (valueName == "max")
        {
            maxVolumeValue = extremeValue;
            maxVolumeValueText.text = maxVolumeValue.ToString("0000");
            print("new max value is " + maxVolumeValue);
            //AudioAnalyzer.instance.maxFreq = extremeValue;
        }
        else
        {
            Debug.LogWarning("Wrong 'valueName' in " + this);
        }
    }
}
