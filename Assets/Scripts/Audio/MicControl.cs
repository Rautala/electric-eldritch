using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class MicControl : MonoBehaviour {
	public static MicControl instance;
	public enum micActivation {
		HoldToSpeak,
		PushToSpeak,
		ConstantSpeak
	}
	
	[HideInInspector]
	public bool virtual3D = false;
	[HideInInspector]
	public float volumeFallOff = 0.3f;
	[HideInInspector]
	public float listenerDistance { get; private set; }
	[HideInInspector]
	public bool ableToHearMic = false;
    [Range(1, 100)]
	public float sensitivity = 10;
	[Range(1,100)]
	public float sourceVolume = 100;//Between 0 and 100
	[HideInInspector]
	public bool GuiSelectDevice = true;
	public micActivation micControl;

    public float controlValue;

    public float minVolumeValue = 1;
    public float maxVolumeValue = 20;


	//
	public string selectedDevice { get; private set; }
	public float loudness { get; private set; } //dont touch

    public Transform scaleableCube;

    public AnimationCurve volumeCurve;

    //

//	private bool micSelected = false;
	private int amountSamples = 1024; //increase to get better average, but will decrease performance. Best to leave it
	private int minFreq, maxFreq;
	
//	private bool focused = true;

	void Awake() {
		instance = this;
	}

	void Start() {
		GetComponent<AudioSource>().loop = true; // Set the AudioClip to loop
		//GetComponent<AudioSource>().mute = false; // Mute the sound, we don't want the player to hear it
		
		for (int i = 0; i < Microphone.devices.Length; i++) {
			print ("device " + i + " is " + Microphone.devices[i]);
		}

        if (Microphone.devices.Length > 0)
        {
            selectedDevice = Microphone.devices[0].ToString();
        }

//		micSelected = true;
		GetMicCaps();
		
		if (Application.isWebPlayer) {
			Application.RequestUserAuthorization(UserAuthorization.Microphone);
			if (Application.HasUserAuthorization(UserAuthorization.Microphone)) {
				selectedDevice = Microphone.devices[0].ToString();
				GetMicCaps();
				StartMicrophone();
//				micSelected = true;
			}
			else return;
		}
	}

	public void GetMicCaps () {
//		Microphone.GetDeviceCaps(selectedDevice, out minFreq, out maxFreq);//Gets the frequency of the device
		Microphone.GetDeviceCaps("", out minFreq, out maxFreq);//Gets the frequency of the device
        
		if ((minFreq + maxFreq) == 0)//These 2 lines of code are mainly for windows computers
			maxFreq = 48000;
	}


	public void StartMicrophone () {
        AudioSource source = GetComponent<AudioSource>();

        source.clip = Microphone.Start("", true, 10, maxFreq);//Starts recording

		while (!(Microphone.GetPosition("") > 0)){} // Wait until the recording has started
		GetComponent<AudioSource>().Play(); // Play the audio source!
		print ("Recordings started to: " + GetComponent<AudioSource>().clip);
        StartCoroutine(UpdateControlValue());
		Invoke ("SetMicSensitivity", 0.5f);
	}

	public void StopMicrophone () {
		GetComponent<AudioSource>().clip = null;
		GetComponent<AudioSource>().Stop();//Stops the audio
		Microphone.End("");//Stops the recording of the device
		
		//MicControl.instance.sensitivity = 0;
		//MicControl.instance.sourceVolume = 0;
	}

    void SetMicSensitivity()
    {
        ///MicControl.instance.sensitivity = 100;
        //MicControl.instance.sourceVolume = 100;
    }

    void Update() {
        if(virtual3D){
			listenerDistance = Vector3.Distance(transform.position, GetComponent<AudioSource>().transform.position);
			GetComponent<AudioSource>().volume = (sourceVolume / 100 / (listenerDistance * volumeFallOff));
		}
		else {
			//GetComponent<AudioSource>().volume = (sourceVolume / 100);
            //loudness = GetAveragedVolume() * sensitivity * (sourceVolume / 10);
            //loudness = GetAveragedVolume() * sensitivity;
		}
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //StartMicrophone();
        }

        //controlValue = volumeCurve.Evaluate(loudness);

        if (scaleableCube != null)
        {
            scaleableCube.localScale = Vector3.Lerp(scaleableCube.localScale, new Vector3(1, controlValue * 5f, 1), Time.deltaTime * 30f);
        }
	}

    IEnumerator UpdateControlValue()
    {
        float value = 0f;
        float averageVolume = 0f;
        int averageVolumeCount = 0;

        while (true)
        {
            averageVolume = GetAveragedVolume() * sensitivity;
            if (averageVolume > 0.05f)
            {
                value += averageVolume;
            }
            else
            {
                value = 0;
            }
            averageVolumeCount++;

            if (averageVolumeCount == 10)
            {
                loudness = ((value / averageVolumeCount) * sensitivity) / maxVolumeValue;
                controlValue = volumeCurve.Evaluate(loudness);
                averageVolumeCount = 0;
                value = 0;
                //print("Conrol value " + controlValue);
            }
            yield return null;
        }
    }

	float GetAveragedVolume() {
		float[] data = new float[amountSamples];
		float a = 0;
		GetComponent<AudioSource>().GetOutputData(data,0);
		foreach(float s in data) {
			a += Mathf.Abs(s);
		}
		return a/amountSamples;
	}

	void OnApplicationFocus(bool focus) {
//		if (enabled) focused = focus;
	}
	
	void OnApplicationPause(bool focus) {
//		if (enabled) focused = focus;
	}
}