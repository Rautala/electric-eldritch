﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuCharacterMover : MonoBehaviour
{
    public static MenuCharacterMover instance;
    private Vector3[] wavePoints;

    public int linePointAmount;
    public float step, waveSpeed, adjustment, animSpeed;

    private float _time = 0, waveLenght = 0, waveHeight = 0, lenght = 0, height = 0;

    public float[] l_minMax, h_minMax;

    private Vector3 targetPos, direction;
    private Quaternion _lookRotation;

    private delegate void ControlMethod();
    private event ControlMethod _method;

    void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start()
    {
        _method += ControlValues;
        wavePoints = new Vector3[linePointAmount];
        for (int i = 0; i < linePointAmount; i++)
        {
            wavePoints[i] = new Vector3(i * step, 0f, 0f);
        }
    }

    public float GetWaveLength()
    {
        return waveLenght;
    }

    // Update is called once per frame
    void Update()
    {
        _time += Time.deltaTime * waveSpeed;
        Wave();
        MovePlayer(wavePoints[0]);
        _method();
    }

    private void ControlValues()
    {
        lenght = Input.GetAxis("Horizontal");
        height = Input.GetAxis("Vertical");
        ChangeValues();
    }

    private void ChangeValues()
    {
        waveHeight = waveHeight + (height * adjustment);
        waveLenght = waveLenght + (lenght * adjustment);

        waveHeight = Clamp(waveHeight, h_minMax);
        waveLenght = Clamp(waveLenght, l_minMax);
    }

    private float Clamp(float _wave, float[] clamps)
    {
        if (_wave > clamps[1]) return clamps[1];
        else if (_wave < clamps[0]) return clamps[0];
        else return _wave;
    }

    private void Wave()
    {
        for (int i = 0; i < linePointAmount; i++)
        {
            float sinWave = Mathf.Sin(_time + (i * step));
            wavePoints[i] = new Vector3((i * step) * (1 + waveLenght), sinWave * (1 + waveHeight), 0f);
        }
    }

    private void MovePlayer(Vector3 pos)
    {
        targetPos = pos;
        if (targetPos != transform.position)
        {
            transform.position = Vector3.Lerp(transform.position, targetPos, _time * animSpeed);
        }
        RotatePlayer();
    }

    private void RotatePlayer()
    {
        direction = (wavePoints[1] - transform.position);
        _lookRotation = Quaternion.LookRotation(direction);
        transform.rotation = Quaternion.Slerp(transform.rotation, _lookRotation, _time * 5f);
    }

}
