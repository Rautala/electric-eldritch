﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scroller : MonoBehaviour {
    private float speed;
    private float _time = 0;
    private Vector3 p { get { return transform.position; } }
	// Use this for initialization
	void Start () {
        speed = BottomSpawner._instance.ScrollingSpeed;
        StartCoroutine(CheckForDestroyment());
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (PlayerControl.instance != null)
        {
            transform.position = new Vector3(p.x - (speed * PlayerControl.instance.GetWaveLength()), p.y, p.z);
        }
        else
        {
            transform.position = new Vector3(p.x - (speed), p.y, p.z);
        }
	}

    private IEnumerator CheckForDestroyment()
    {
        while(true)
        {
            if(BottomSpawner._instance.endValue > transform.position.x)
            {
                BottomSpawner._instance.SpawnNow();
                Destroy(gameObject);
            }
            yield return null;
        }
    }
}
