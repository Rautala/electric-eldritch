﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Timer : MonoBehaviour {
    
    float time = 0f;
    public Text timerText;

	// Use this for initialization
	void Start () {
        
    }

    System.TimeSpan span;

    // Update is called once per frame
    void Update () {
        if (EnemyManager.gameOn)
        {
            time += Time.deltaTime;
            span = System.TimeSpan.FromSeconds(time);
            timerText.text = span.Minutes.ToString("00") + ":" + span.Seconds.ToString("00");
        }
        
	}
}
