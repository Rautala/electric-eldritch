﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyManager : MonoBehaviour {

    public ObstacleData[] obstacleDatabase;
    public Text clicksNeededText;
    public Text turboText;
    public Slider turboSlider;
    public int baseClicksNeeded = 30;
    public float penaltyReductionTimeInSeconds = 0;
    public int penaltyIncrease = 0;    
    public int penaltyDecrease = 0;
    public int clickStorageMax = 0;
    public float maxTurboValue = 0.3f;
    public float turboRechargeTimeInSeconds = 0;
    public string clicksNeededBaseText = "";
    public AudioClip baseClip;
    public AudioSource clipAudioSource;
    public static float turboModifier = 1;
    public static bool gameOn = false;

    int clicksNeededForNextObstacle = 0;
    int currentPenalty = 0;
    float lastPenaltyReduction;
    float lastTurboUsed;
    float lastValue = 0;
    float turbo = 0;

    void Start()
    {
        setGameState(true);
    }

    public void setGameState(bool isItOn)
    {
        if (isItOn && !gameOn)
        {
            clicksNeededText.enabled = true;
            turboSlider.gameObject.SetActive(true);
            turboText.gameObject.SetActive(true);
            gameOn = true;
            StartCoroutine(EnemyController());
        }
        else if (!isItOn)
        {
            clicksNeededText.enabled = false;
            turboSlider.enabled = false;
            turboSlider.gameObject.SetActive(false);
            turboText.gameObject.SetActive(false);
            gameOn = false;
        }
    }

	public void createNewObstacle(int id)
    {
        GameObject newObstacle = Instantiate(obstacleDatabase[id].prefab);
        clicksNeededForNextObstacle += baseClicksNeeded + obstacleDatabase[id].clicksNeededAddition + currentPenalty;
        currentPenalty += penaltyIncrease;

        if (clipAudioSource != null)
            clipAudioSource.PlayOneShot(obstacleDatabase[id].clipToPlay);

        if (Input.GetButton("Modifier"))
            newObstacle.GetComponent<BasicMove>().setStartingValues(true);
        else
            newObstacle.GetComponent<BasicMove>().setStartingValues(false);
    }

    bool getValidBarClick()
    {
        return ((Input.GetAxis("Click") > 0 || Input.GetAxis("Click") < 0) && Input.GetAxis("Click") != lastValue);
    }

    IEnumerator EnemyController()
    {
        lastTurboUsed = Time.fixedTime;
        lastPenaltyReduction = Time.fixedTime;
        lastValue = 0;
        while (gameOn)
        {
            clicksNeededText.text = clicksNeededBaseText + clicksNeededForNextObstacle;
            turboSlider.value = turbo;

            if ((Input.GetButtonDown("Mouse2") || (Input.GetButton("Fire1")) && getValidBarClick()) && clicksNeededForNextObstacle <= 0)
                createNewObstacle(0);
            else if ((Input.GetButton("Fire2") && getValidBarClick()) && clicksNeededForNextObstacle <= 0)
                createNewObstacle(1);
            else if ((Input.GetButton("Fire3") && getValidBarClick()) && clicksNeededForNextObstacle <= 0)
                createNewObstacle(2);
            else if ((Input.GetButton("Fire4") && getValidBarClick()) && clicksNeededForNextObstacle <= 0)
                createNewObstacle(3);
            else if (clicksNeededForNextObstacle > clickStorageMax && (Input.GetButtonDown("Mouse1") || getValidBarClick()))
            {
                clicksNeededForNextObstacle--;
                if (clipAudioSource != null && baseClip != null)
                    clipAudioSource.PlayOneShot(baseClip);
            }

            if (Input.GetAxis("Slider") != 0 && Input.GetAxis("Slider") > -1 && turbo > 0.01)
            {
                float turboValue = (Input.GetAxis("Slider") + 1) / 2;
                turboModifier = 1 + turboValue * maxTurboValue;
                turbo -= turboValue * 0.5f * Time.deltaTime;
                lastTurboUsed = Time.fixedTime;
            }
            else if (Input.GetAxis("Slider") == -1 && Time.fixedTime - lastTurboUsed > turboRechargeTimeInSeconds && turbo < 1)
            {
                turbo += 0.25f * Time.deltaTime;
                if (turbo > 1)
                    turbo = 1;
                turboModifier = 1;
            }
            else 
            {
                turboModifier = 1;
            }

            lastValue = Input.GetAxis("Click");

            if (currentPenalty > 0 && Time.fixedTime - lastPenaltyReduction > penaltyReductionTimeInSeconds)
            {
                currentPenalty -= penaltyDecrease;
                if (currentPenalty < 0)
                    currentPenalty = 0;

                lastPenaltyReduction = Time.fixedTime;
            }
            else if (currentPenalty == 0 && clicksNeededForNextObstacle > clickStorageMax && Time.fixedTime - lastPenaltyReduction > penaltyReductionTimeInSeconds)
            {
                clicksNeededForNextObstacle -= penaltyDecrease * 2;
                if (clicksNeededForNextObstacle < clickStorageMax)
                    clicksNeededForNextObstacle = clickStorageMax;

                lastPenaltyReduction = Time.fixedTime;
            }            

            yield return null;
        }
    }

}