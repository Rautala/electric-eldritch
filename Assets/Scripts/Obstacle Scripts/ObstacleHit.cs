﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ObstacleHit : MonoBehaviour {

    public GameObject playerLine;
    public PlayerControl control;
    public EnemyManager enemies;
    public Animator playerAnimator;
    public Collider playerCollider;

    void OnTriggerEnter(Collider other)
    { 
        if (other.gameObject.tag == "Enemy")
        {
            control.enabled = false;
            enemies.setGameState(false);
            enemies.clicksNeededText.enabled = true;
            enemies.clicksNeededText.text = "Press Escape to restart";      
            playerAnimator.enabled = false;
            playerCollider.enabled = false;
        }
    }

    void Update()
    {
        if (Input.GetButtonUp("Cancel"))
            SceneManager.LoadScene(0);
    }

}
