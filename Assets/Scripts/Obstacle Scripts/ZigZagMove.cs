﻿using System.Collections.Generic;
using UnityEngine;

public class ZigZagMove : BasicMove {

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (moveOn)
            advancedMove(1);        
    }    

    public override void setStartingValues(bool downLow)
    {
        base.setStartingValues(downLow);

        calculateRoute();
    }

    public override void calculateRoute()
    {
        route = new List<Vector3>();

        if (transform.position.y == maxYValue)
            route.Add(new Vector3(transform.position.x - xDistanceBetweenMinAndMax, minYValue, transform.position.z));
        else
            route.Add(new Vector3(transform.position.x - xDistanceBetweenMinAndMax, maxYValue, transform.position.z));

        float currentOffset = xDistanceBetweenMinAndMax;
        int routeIndex = 0;

        while (transform.position.x - currentOffset > endPoint.xCord - 0.02)
        {
            currentOffset += xDistanceBetweenMinAndMax;
            if (route[routeIndex].y == minYValue)
                route.Add(new Vector3(transform.position.x - currentOffset, maxYValue, transform.position.z));
            else
                route.Add(new Vector3(transform.position.x - currentOffset, minYValue, transform.position.z));

            routeIndex++;
            
        }
    }

}
