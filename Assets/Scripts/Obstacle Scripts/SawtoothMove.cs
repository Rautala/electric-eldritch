﻿using System.Collections.Generic;
using UnityEngine;

public class SawtoothMove : BasicMove {
        
    bool lastMoveWasX = false;

    // Update is called once per frame
    void Update()
    {
        if (moveOn)
            advancedMove(1);
    }

    public override void setStartingValues(bool downLow)
    {
        base.setStartingValues(downLow);
        calculateRoute();
    }

    public override void calculateRoute()
    {
        route = new List<Vector3>();

        float currentOffset = xDistanceBetweenMinAndMax;
        if (transform.position.y == maxYValue)
            route.Add(new Vector3(transform.position.x - currentOffset, minYValue, transform.position.z));
        else
            route.Add(new Vector3(transform.position.x - currentOffset, maxYValue, transform.position.z));

        currentOffset += xDistanceBetweenMinAndMax;

        bool lastMoveWasX2 = false;
        int routeIndex = 0;

        while (transform.position.x - currentOffset > endPoint.xCord - 0.02)
        {
            if (lastMoveWasX2)
            {
                if (route[routeIndex].y == maxYValue)
                    route.Add(new Vector3(transform.position.x - currentOffset, minYValue, transform.position.z));
                else
                    route.Add(new Vector3(transform.position.x - currentOffset, maxYValue, transform.position.z));

                currentOffset += xDistanceBetweenMinAndMax;
            }
            else
            {
                if (route[routeIndex].y == maxYValue)
                    route.Add(new Vector3(route[routeIndex].x, minYValue, transform.position.z));
                else
                    route.Add(new Vector3(route[routeIndex].x, maxYValue, transform.position.z));
            }

            routeIndex++;
            lastMoveWasX2 = !lastMoveWasX2;
        }
    }

}
