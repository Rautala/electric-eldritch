﻿using System.Collections.Generic;
using UnityEngine;

public class BasicMove : MonoBehaviour
{
    public LineRenderer lineRend;
    public Color lineColor;
    public float speed;
    public TargetData endPoint;
    public float maxYValue;
    public float minYValue;
    public float xDistanceBetweenMinAndMax;
    public bool randomize;
    public int maxRouteLinesToShow = 3;

    protected List<Vector3> route = new List<Vector3>();
    protected List<Vector3> newRoute = new List<Vector3>();
    protected Vector3 target;
    protected bool moveOn = false;
    

    // Update is called once per frame
    void Update()
    {
        if (moveOn)        
            basicMove(newRoute[0], 1);
    }

    public virtual void basicMove(Vector3 moveTarget, float secondarySpeedMultiplier)
    {
        updateRoute();
        transform.position = Vector2.MoveTowards(transform.position, moveTarget, speed * secondarySpeedMultiplier * EnemyManager.turboModifier * Time.deltaTime);

        bool targetReached = true;

        if (endPoint.useX && endPoint.xCord < transform.position.x)
            targetReached = false;
        else if (endPoint.useY && endPoint.yCord < transform.position.y)
            targetReached = false;
        else if (endPoint.useZ && endPoint.zCord < transform.position.z)
            targetReached = false;
        else if (!endPoint.useX && !endPoint.useY && !endPoint.useZ)
            targetReached = false;

        if (targetReached)
            Destroy(gameObject);

        if (lineRend != null)
        {
            Vector3[] linePositions = new Vector3[newRoute.Count + 1];
            linePositions[0] = transform.position;

            int index = 1;
            foreach (Vector3 routePoint in newRoute)
            {
                linePositions[index++] = routePoint;
            }

            if (maxRouteLinesToShow < linePositions.Length)
                lineRend.numPositions = maxRouteLinesToShow;
            else
                lineRend.numPositions = linePositions.Length;

            lineRend.SetPositions(linePositions);
        }
    }

    public void advancedMove(float secondarySpeedMultiplier)
    {
        if (newRoute.Count > 0)
        {
            basicMove(newRoute[0], secondarySpeedMultiplier);

            if (Vector3.Distance(transform.position, newRoute[0]) < 0.01)
            {
                route.RemoveAt(0);
                newRoute.RemoveAt(0);
            }
        }
        else
            basicMove(new Vector3(endPoint.xCord, transform.position.y, transform.position.z), secondarySpeedMultiplier);        
    }

    public virtual void setStartingValues(bool downLow)
    {
        if (lineRend != null)
            lineRend.material.SetColor("Albedo", lineColor);

        if (randomize)
        {
            float midway = minYValue + ((maxYValue - minYValue) / 2);

            if (downLow)
                transform.position = new Vector3(transform.position.x, Random.Range(minYValue, midway), transform.position.z);
            else
                transform.position = new Vector3(transform.position.x, Random.Range(midway, maxYValue), transform.position.z);
        }
        else
        {
            if (downLow)
                transform.position = new Vector3(transform.position.x, minYValue, transform.position.z);
            else
                transform.position = new Vector3(transform.position.x, maxYValue, transform.position.z);
        }

        calculateRoute();
        newRoute = route;
        moveOn = true;
    }

    public virtual void calculateRoute()
    {
        route.Add(new Vector3(endPoint.xCord, transform.position.y, 0));
    }

    public void updateRoute()
    {
        for (int i = 0; i < newRoute.Count; i++)
        {
            newRoute[i] = new Vector3(route[i].x - 10 * PlayerControl.instance.GetNormalizedWaveLength(), route[i].y, route[i].z);
        }
    }

}
