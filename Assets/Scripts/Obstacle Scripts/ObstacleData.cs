﻿using UnityEngine;

[System.Serializable]
public class ObstacleData
{
    public GameObject prefab;
    public AudioClip clipToPlay;
    public int clicksNeededAddition;
}

[System.Serializable]
public class TargetData
{
    public bool useX = false;
    public bool useY = false;
    public bool useZ = false;
    public int xCord = 0;
    public int yCord = 0;
    public int zCord = 0;
}