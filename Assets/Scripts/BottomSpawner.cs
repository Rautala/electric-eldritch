﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottomSpawner : MonoBehaviour {
    public static BottomSpawner _instance;

    public GameObject[] spawnableBottoms;

    public float endValue { get { return transform.position.x * -1f; } }
    public GameObject lastPiece;
    private Vector3 lp_p { get { return lastPiece.transform.position; } }

    public float ScrollingSpeed;

    private float _time = 0;
    // Use this for initialization
    private void Awake()
    {
        _instance = this;
    }

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        _time = Time.deltaTime;
	}

    private GameObject InstantiateBottom()
    {
        return Instantiate<GameObject>(spawnableBottoms[Random.Range(0, spawnableBottoms.Length)], 
            new Vector3(lp_p.x + 100f, transform.position.y,transform.position.z), transform.rotation);
    }

    public void SpawnNow()
    {
        GameObject go = InstantiateBottom();
        lastPiece = go;      
    }
}
