﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuButtonMover : MonoBehaviour {

    public float movespeed = 0.5f;

    public float movement = 25;

    Vector3 originalPos;
    Vector3 originalRot;

    RectTransform rectTransform;

    public bool rotate = true;

	// Use this for initialization
	void Start () {
        rectTransform = (RectTransform)transform;

        originalPos = rectTransform.localPosition;
        originalRot = rectTransform.localEulerAngles;

        movespeed += Random.Range(0.01f, 0.05f);

        StartCoroutine(GoUp());
        if (rotate)
        {
            StartCoroutine(RotLeft());
        }
	}
	
	IEnumerator GoUp()
    {
        while (rectTransform.localPosition.y < originalPos.y + movement)
        {            
            rectTransform.localPosition = new Vector3(originalPos.x, rectTransform.localPosition.y + movespeed, originalPos.z);
            yield return null;
        }
        
        StartCoroutine(GoDown());
    }

    IEnumerator GoDown()
    {
        while (rectTransform.localPosition.y > originalPos.y - movement)
        {
            rectTransform.localPosition = new Vector3(originalPos.x, rectTransform.localPosition.y - movespeed, originalPos.z);
            yield return null;
        }
        
        StartCoroutine(GoUp());
    }


    IEnumerator RotLeft()
    {
        while (rectTransform.localEulerAngles.y < originalRot.y + movement)
        {
            rectTransform.localEulerAngles = new Vector3(originalRot.x, rectTransform.localPosition.y + movespeed, rectTransform.localPosition.z + movespeed);
            yield return null;
        }
        
        StartCoroutine(RotRight());
    }


    IEnumerator RotRight()
    {
        while (rectTransform.localEulerAngles.y > originalRot.y - movement)
        {
            rectTransform.localEulerAngles = new Vector3(originalRot.x - movespeed, rectTransform.localPosition.y - movespeed, rectTransform.localPosition.z - movespeed);
            yield return null;
        }
        
        StartCoroutine(RotLeft());
    }
}
