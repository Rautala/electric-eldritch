﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour {
    public static PlayerControl instance;
    private Vector3[] wavePoints;
    public LineRenderer lr;

    public int linePointAmount;
    public float step,waveSpeed, adjustment, animSpeed;

    private float _time = 0, waveLenght = 0, waveHeight = 0, lenght = 0, height = 0;

    public float[] l_minMax, h_minMax;

    private Vector3 targetPos, direction;
    private Quaternion _lookRotation;

    private delegate void ControlMethod();
    private event ControlMethod _method;

    private bool usePitch = false;

    void Awake()
    {
        instance = this;
    }

    // Use this for initialization
	void Start () {
        if (MicControl.instance == null) _method += ControlValues;
        else _method += ControlValuesMic;

        usePitch = (PlayerPrefs.GetInt("pitch") == 0) ? false : true;
        print("using pitch " + usePitch);
        SetupLine();
        //MicControl.instance.StartMicrophone();
        Invoke("StartMicrophone", 0.5f);
    }

    void StartMicrophone()
    {
        if (MicControl.instance != null)
        {
            MicControl.instance.StartMicrophone();
        }
    }

    public float GetWaveLength()
    {
        return waveLenght;
    }

    public float GetNormalizedWaveLength()
    {
        return (waveLenght - l_minMax[0]) / (l_minMax[1] - l_minMax[0]);
    }

    // Update is called once per frame
    void Update () {
        _time += Time.deltaTime * waveSpeed;
        Wave();
        MovePlayer(wavePoints[0]);
        _method();
	}

    private void ControlValuesMic()
    {
        height = MicControl.instance.controlValue;

        if (usePitch)
        {
            if (AudioAnalyzer.instance.AnalyzeSound() > 10)
            {
                lenght = AudioAnalyzer.instance.Frequency();
            }
            else
            {
                lenght = 0;
            }
        }
        else
        {
            lenght = MicControl.instance.controlValue;
        }
        ChangeValues();
    }

    private void ControlValues()
    {
        lenght = Input.GetAxis("Horizontal");
        height = Input.GetAxis("Vertical");
        ChangeValues();
    }

    private void ChangeValues()
    {
        waveHeight = waveHeight + (height * adjustment);
        waveLenght = waveLenght + (lenght * adjustment);

        waveHeight = Clamp(waveHeight, h_minMax);
        waveLenght = Clamp(waveLenght, l_minMax);
    }

    private float Clamp(float _wave, float[]clamps)
    {
        if (_wave > clamps[1]) return clamps[1];
        else if (_wave < clamps[0]) return clamps[0];
        else return _wave;
    }
    
    private void SetupLine()
    {
        wavePoints = new Vector3[linePointAmount];
        for(int i=0;i < linePointAmount;i++)
        {
            wavePoints[i] = new Vector3(i * step, 0f, 0f);
        }
        lr.numPositions = wavePoints.Length;
        lr.SetPositions(wavePoints);      
    }

    private void Wave()
    {
        for(int i=0; i<linePointAmount;i++)
        {
            float sinWave = Mathf.Sin(_time + (i*step));
            wavePoints[i] = new Vector3((i * step) * (1 + waveLenght), sinWave* (1 + waveHeight), 0f);
        }
        lr.SetPositions(wavePoints);
    }

    private void MovePlayer(Vector3 pos)
    {
        targetPos = pos;
        if(targetPos != transform.position)
        {
            transform.position = Vector3.Lerp(transform.position, targetPos, _time * animSpeed);
        }
        RotatePlayer();
    }

    private void RotatePlayer()
    {
        direction = (wavePoints[1] - transform.position);
        _lookRotation = Quaternion.LookRotation(direction);
        transform.rotation = Quaternion.Slerp(transform.rotation, _lookRotation, _time * 5f);
    }

}
